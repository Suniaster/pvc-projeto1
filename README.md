# PVC-Projeto1

Trabalho 1 da matéria de Principios da visão computacional 2019/1
Feito por Thiago Chaves Monteiro
Link do repositório: https://gitlab.com/Suniaster/pvc-projeto1

## Requisitos

Para o uso do progama é necessário que a instalação do software python na versão 3.4.1 tenha sido feito na maquina, assim como da biblioteca opencv, versão 3.3.1.

## Especificações

O programa é divido em 4 módulos. Cada um deles cumpre uma das exigências pedidas para a criação do programa.

O módulo 1 e 2 abrem uma imagem na pasta data e opera sobre ela. Caso haja a necessidade de mudar essa imagem, é necessário botar a imagem na pasta data e em seguida ir na pasta *src* e mudar, no arquivo *req1.py* ou *req2.py*, a linha:

```python
img = cv2.imread("data/image.jpg", -1)
```

Para:

```python
img = cv2.imread("data/*nome_da_sua_imagem*", -1)
```

Já os módulos 3 e 4 operam sobre vídeos. No módulo 3 é usado um video da pasta data, caso haja necessidade de muda-lo, basta fazer processo  similar ao descrito anteriormente porém no arquivo *req3.py*. Já o módulo 4 opera sobre a webcam do computador, portanto é necessário que haja um dispositivo de aquisição de video conectado ao computador.

No arquivo ImageControl.py é definida a classe responsável por realizar as operações sobre as imagens ou videos e armazenar o conteúdo deles. Já os arquivos r1.py, r2.py, r3.py e r4.py são apenas scripts para abrir os arquivos desejados e utilizam a lógica criada na classe ImageController para realizar seus processos. Cada um desses arquivos anteriormente citados cumpre uma das requisições do trabalho.

## Utilização

Para usar qualquer um dos módulos, basta rodar o arquivo start.sh passando como parametro a string r*n*, substituindo *n* pelo número do módulo desejado.
Exemplo para rodar o módulo 1:

```bash
./start.sh r1
```

Exemplo para rodar o módulo 3:

```bash
./start.sh r3
```