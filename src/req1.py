import numpy as np
import cv2
from ImageControl import ImageController

img = cv2.imread("data/image.jpg", -1)

control = ImageController('image', img)
cv2.imshow(control.name, control.img)

cv2.setMouseCallback('image',control.MCB_PrintMouse)

cv2.waitKey(0)

cv2.destroyAllWindows()
