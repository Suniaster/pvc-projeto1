import numpy as np
import cv2
import math  
from ImageControl import ImageController

# Pegando imagem
img = cv2.imread("data/image.jpg",-1)

control = ImageController('image',img)

cv2.imshow(control.name, control.img)

cv2.setMouseCallback(control.name, control.MCB_Paint)

cv2.waitKey(0)

cv2.destroyAllWindows()
