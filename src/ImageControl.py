import numpy as np
import cv2
import math

class ImageController:
    def __init__(self, name, img=np.zeros(1), video=False):
        # Parando execução caso haja parametros invalidos
        if type(img) == type(None):
            raise Exception("Imagem inexistente passada de argumento para ImageController.\n Cheque o nome, a extensão e o path da imagem.")


        self.basePx = (-1,-1,-1)
        self.mouseCrd = {
            "x": -1,
            "y": -1
        }
        # Image Variables
        self.name = name
        self.img = img

        # Colocando valores padrão
        self.defaultColor = (0,0,255)
        self.isGray = False
        self.MCB_Paint = self.MCB_PaintColor
        self.hasSelected = False
        self.baseDif = 13

        # Mudando valores padrão caso necessário
        if video != False:
            self.video = video
            self.isFinished = False
        if len(img.shape)>1 :
            if len(img.shape) == 2:
                self.isGray = True
                self.MCB_Paint =  self.MCB_PaintGray
            else:
                # No caso de imagens com canal alfa, a cor padrão também deve possuir alfa
                if img.shape[2] == 4:
                    self.defaultColor = (0,0,255,1)

    def attMousePos(self,img,x,y):
        """Salva cordenadas do mouse no dicionario e printa no terminal informações sobre o local"""
        self.mouseCrd = {
            "x": x,
            "y": y
        }
        if self.isGray:
            print('X:'+str(x)+', Y:'+str(y)+', Luminosidade: '+str(img[y,x]))
        else:
            print('X:'+str(x)+', Y:'+str(y)+', RGB:'+str(img.item(y,x,2))+' '+str(img.item(y,x,1))+' '+str(img.item(y,x,0)))

    def changeBasePix(self, img, x,y):
        """Muda o pixel base de comparação"""
        self.basePx = img[y,x].copy()

    def paintImageColor(self, baseImg, baseDif):
        """
            !!!Necessario o que atributo basePx da instancia tenha sido inicializado\n
                -Funções de callback da classe fazem isso por padrão
            Pinta todos os pixeis da image que possuem distancia euclidiana menor que o dif.
            Recebe como parametro:
                @baseImg: objeto imagem a ser pintada
                @baseDif: inteiro que será a base da comparação entre as distancias
            Retorna: array que representa uma imagem
        """
        
        # Criando matriz de distancias
        mask = np.square(baseImg.astype(int) - self.basePx)
        mask = np.sum(mask, axis=2)
        mask = np.sqrt(mask)

        img = baseImg.copy()
        # np.where((mask<baseDif)) retorna um array de uma tupla de dois arrays
        # cada um desses arrays corresponde a uma posição, x na primeira posição e y na segunda
        # exemplo de retorno de np.where((mask<baseDif)) : (array[89], array[89])
        # assim, o n-ésimo elemento de cada um desses arrays corresponde a um pixel da img.
        # Por ter passado pela condição (mask<baseDif), oq foi retornado são apenas os pixeis necessários
        # que sejam substituidos.
        # Assim, "baseImg[np.where((mask<baseDif))] = self.defaultColor" troca a cor de todos os pixeis 
        # com distância menor do que o que foi passado como paramêtro 
        img[np.where((mask<baseDif))] = self.defaultColor

        return img

    def paintImageGray(self, baseImg, baseDif):
        """
            !!!Necessario o que atributo basePx da instancia tenha sido inicializado\n
                -Funções de callback da classe fazem isso por padrão
            Pinta todos os pixeis da image que possuem distancia euclidiana menor que o dif.
            Recebe como parametro:
                @baseImg: objeto imagem a ser pintada
                @baseDif: inteiro que será a base da comparação entre as distancias
            Retorna: array que representa uma imagem
        """
        
        # fazendo distancia absoluta de todos os pixeis em relação ao pixel base
        mask = np.absolute(baseImg.astype(int) - self.basePx)
        
        img = baseImg.copy()

        # É necessário transformar a imagem que anteriormente era (N x M) em
        # (N x M x 3) para que os pixeis vermelhos possam ser inseridos
        img = np.repeat(img[...,None], 3, axis=2)

        # np.where((mask<baseDif)) retorna tupla de dois arrays
        # cada um desses arrays corresponde a uma posição, x e y da imagem
        # exemplo de retorno de np.where((mask<baseDif)): ([0,3,4,1], [1,4,5,3])
        # assim, o n-ésimo elemento de cada um desses arrays corresponde a um pixel da img.
        # Usando como base o exemplo anterior, o tanto o valor [0,1] quanto o [3,4] são indices
        # da imagem retornados
        # Por ter passado pela condição (mask<baseDif), oq foi retornado são apenas os pixeis necessários
        # que sejam substituidos.
        # Assim, "baseImg[np.where((mask<baseDif))] = self.defaultColor", troca a cor de todos os pixeis 
        # com distância menor do que o que foi passado como paramêtro 
        img[np.where((mask<baseDif))] = self.defaultColor

        
        return img
    ####################### Image Handling ############################

    def MCB_PrintMouse(self,event,x,y,flags,param):
        if event == cv2.EVENT_LBUTTONDOWN:
            self.attMousePos(self.img,x,y)

    def attImage(self, img):
        """Atualiza a tela em que a imagem está sendo mostrada"""
        cv2.imshow(self.name, img)

    def MCB_PaintGray(self,event,x,y,flags,param):
        """Callback do mouse Para pintar a imagem toda vez que for dado double clique"""
        if event == cv2.EVENT_LBUTTONDOWN:
            self.attMousePos(self.img,x,y)
            # Fazendo processamento e atualizando imagem
            self.changeBasePix(self.img,x,y)

            self.newImg = self.paintImageGray(self.img, self.baseDif)
            
            self.attImage(self.newImg)
            print('')

    def MCB_PaintColor(self,event,x,y,flags,param):
        """Callback do mouse Para pintar a imagem toda vez que for dado double clique"""
        if event == cv2.EVENT_LBUTTONDOWN:
            self.attMousePos(self.img,x,y)
            # Fazendo processamento e atualizando imagem
            self.changeBasePix(self.img,x,y)
            self.newImg = self.paintImageColor(self.img, self.baseDif)
            self.attImage(self.newImg)
            print('')

    ####################### Video Handling ############################


    def changeFrame(self):
        """Muda o frame do video"""
        ret, self.frame = self.video.read()
        return ret

    def MCB_GetBasePx(self,event,x,y,flags,param):
        """Callback do mouse para salvar o pixel base de comparação"""
        if event == cv2.EVENT_LBUTTONDOWN:
            self.attMousePos(self.frame,x,y)
            self.changeBasePix(self.frame,x,y)
            self.hasSelected = True

    def paintFrameRed(self):
        """Pinta os pixeis do frame de vermelho"""
        if self.hasSelected:
            self.frame = self.paintImageColor(self.frame,13)

