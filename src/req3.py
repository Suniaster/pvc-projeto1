import numpy as np
import cv2
import math  
from ImageControl import ImageController

# geting Video
cap = cv2.VideoCapture('data/video.mp4')
control = ImageController('frame', np.zeros(1), video=cap)

while(True):

    if not control.changeFrame():
        break

    control.paintFrameRed()
    cv2.imshow(control.name,control.frame)
    cv2.setMouseCallback(control.name, control.MCB_GetBasePx)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cap.release()
cv2.destroyAllWindows()